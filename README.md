# Fuzzy Search Endpoint / Word Search

## Problem

Write an http service that provides an endpoint for fuzzy search / autocomplete of English words.

* Constraints were specified for the problem. (Not highlighed here).

## System Configuration

* Macbook Pro 2015 (i5,4 Core, 8 GB RAM)
* Python3

## Tradeoffs and Notes

Tradeoffs and Notes (for the specific system):

    * Speed increases with word length.
    * Repeated searches are being cached with LRU cache, enables blazing fast speed at around 0.001 ms.
    * Speed is around 80-150 ms for 3-4 letter words and above.
    * Around 150-300 ms for one letter words
    * Detects some amount of typo's since based out of greedy regex pattern.
    * Better results can be achieved with levenshtein ratio being checked, in the else statement if time tradeoff is acceptable.
    * Based on O(logn) for the search within the regex.

## Installation

* virtualenv --python=python3 .venv
* pip install -r requirements.txt
* python app.py (Runs on default port of 5000)
* Browse to http://localhost:5000/ for UI
* For the API endpoint : http://localhost:5000/search?word=test

## Tests

* Comment out decorator for functools lru_cache.
* pytest tests/
