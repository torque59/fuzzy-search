import re
import time
#import sys
#import logging
import pandas
import functools
#import cProfile
from flask import Flask
from flask import jsonify, request, render_template


app = Flask(__name__)


#logging.basicConfig(stream=sys.stdout, level=logging.INFO) -> Stick to print here.


# Pandas loads csv much faster.Not an issue here since file is being loaded only once during start.
try:
    pd = pandas.read_csv("word_search.tsv", sep='\t', usecols=[0])
    collection = pd.to_csv(None, header=False, index=False).split('\n')  # Convert to list for easier operations.
except FileNotFoundError:
    print("[-] File Not found.")


@functools.lru_cache(maxsize=100) # Used to speed up repeated searches :)
def fuzzy_search(search_string,collections=None):
    """
    Fuzzy search to perform string search, based on regex greedy pattern which is faster.(Assuming user word/letter input)

    Possible algorithms = Levenshtein distance (difflib, fuzzy wuzzy) , n-gram checks => which are considerably very slow (~ 1000 ms per search )

    System used -> Macbook Pro Intel i5, 4 core with 8GB ram

    Tradeoffs and Notes (for the specific system):
    * Speed increases with word length.
    * Repeated searches are being cached with LRU cache, enables blazing fast speed at around 0.001 ms.
    * Speed is around 80-150 ms for 3-4 letter words and above.
    * Around 150-300 ms for one letter words
    * Detects some amount of typo's since based out of greedy regex pattern.
    * Better results can be achieved with levenshtein ratio being checked, in the else statement if time tradeoff is acceptable.
    * Based on O(log n) for the search within regex.

    """
    outcomes = []
    greedy_pattern = '.*?'.join(search_string.lower())   #regex pattern  -> greedy regex -> Joins a.*?b.*?c

    regex = re.compile(greedy_pattern)  # Compile regex.
    for item in collection:
        match = regex.search(item)   # Checks if the current item matches the regex.
        if match:
            outcomes.append((len(match.group()), match.start(), item)) # tuple to get len of match and sort to the nearest matching start.
        # else:
         #   if Levenshtein.ratio(user_input,item) > 0.85: # Levenshtein makes the search slower
          #     suggestions.append((0,0,item))
    return [x for _, _, x in sorted(outcomes)]


@app.route('/search')
def search():
    """
    API endpoint which does the search call.
    """
    word = request.args.get("word")

    start = time.time() # Use time than cProfile for better readability.
    output = fuzzy_search(word)[:25] # First 25 results only.
    end = time.time()

    print("Time taken for function call fuzzy_search : %i \n"%(int(round(1000*(end - start)))))
    return jsonify(matching_results=output)

@app.route('/')
def index():
    """
    Index page to search the words for.
    """
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True) # Development use.
