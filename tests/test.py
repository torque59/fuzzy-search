import pytest
import functools
from ..app import fuzzy_search

def sample_data():
    return ["procase", "prochaska", "procrastinate", "procrastinated", "procrastinating", "procrastination", "procrastinator",  "procrastinators", "sigprocmask",
	"reciprocals",
    "pyroclastic",
    "reciprocates",
    "pornocams",
    "pornocamsfree",
    "prochains",
    "proclaims",
    "gephyrocapsa",
    "freepornocams",
    "prochaines",
    "productcasts",
    "provincias",
    "aptbroadcast",
    "getprocaddress",
    "parochialism",
    "periodicals"
  ]

def test_fuzzy_search():
    """
    Test basic case whether string returns the corresponding data
    """
    data = sample_data()
    #@functools.lru_cache(maxsize=100)
    final = fuzzy_search("proc",data)
    if "proc" in final[0]:
        check = True
    else:
        check = False
	assert check

